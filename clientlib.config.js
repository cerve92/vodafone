/**
 * Build Windows
 * set PUBLIC_URL=http://localhost:4502/etc.clientlibs/wknd/clientlibs-webebu-react/clientlib-reactbuild/resources&&npm run build:dev
 * 
 * Build Linux
 * PUBLIC_URL=http://localhost:4502/etc.clientlibs/wknd/clientlibs-webebu-react/clientlib-reactbuild/resources npm run build:dev
 */

module.exports = {
    // default working directory (can be changed per 'cwd' in every asset option)
    context: __dirname,
 
    // path to the clientlib root folder (output)
    //clientLibRoot: "./../aem-guides-wknd/ui.apps/src/main/content/jcr_root/apps/wknd/clientlibs-webebu-react/",
    clientLibRoot: "devops/aem-clientlib-generator/dist",
 
    libs: {
        name: "clientlib-reactbuild",
        allowProxy: true,
        categories: ["webebu.reactbuild"],
        serializationFormat: "xml",
        jsProcessor: ["default:none", "min:gcc;compilationLevel=whitespace"],
        assets: {
            js: [
                "build/static/js/*.js"
                //"build/static/js/runtime*.*.js",
                //"build/static/js/*20.c573e594*.js",
                //"build/static/js/main.*.chunk.js"
            ],
            css: [
                "build/static/css/main.*.chunk.css"
            ],
            resources: [
                //{src: "build/static/js/*.js", dest: "static/js/"},
                {src: "build/static/css/*.css", dest: "static/css/"},
                {src: "build/static/media/*", dest: "static/media/"}
            ]
        }
    }
};