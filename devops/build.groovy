#!groovy
 
// All methods inside this file are invoked by a Jenkins Shared Library in
// order to make complete all pipeline steps.
 
// testBuild() is a required method, used to execute the compilation of sources
// and unit test.
// Into this method a node must be specified (ex. maven or nodejs), where it
// must be retrieved some environment variables such as MAVEN_SETTING_ID and
// STASH_ID.
 
// PARAMETER: MAVEN_SETTING_ID is used in order to retrieve with
// configFileProvider method the maven settings.xml file previously configured
// inside Jenkins
 
// PARAMETER: STASH_ID is used in order to retrieve the tar sources inside the
// newest creade slave node
 
// inside the configFileProvider method must be inserted, via shell command
// line, the commands that compile the sources and execute unit tests
// def testBuild(def argsMap) {
def testBuild(def argsMap) {
    this.printVariables(argsMap)
 
    String customMvnSettings = "${argsMap['MAVEN_SETTINGS_ID']}"
    String mavenSettingId = (customMvnSettings?.trim()) ? customMvnSettings : "MAVEN_SETTINGS"
 
 
    configFileProvider([configFile(fileId: mavenSettingId, variable: "MAVEN_SETTINGS")]) {
        sh returnStatus: true, script: "mvn clean test -s ${MAVEN_SETTINGS}"
    }
}
 
// packageBuild() is a required method, used to execute the compilation of
// sources and the artifact packaging (without unit tests).
// Into this method a node must be specified (ex. maven or nodejs), where it
// must be retrieved some environment variables such as MAVEN_SETTING_ID,
// STASH_ID, OCP_APPLICATION_NAME, OCP_BUILD_PROJECT, OCP_IMAGE_VERSION.
 
// PARAMETER: MAVEN_SETTING_ID is used in order to retrieve with
// configFileProvider method the maven settings.xml file previously configured
// inside Jenkins
 
// PARAMETER: STASH_ID is used in order to retrieve the tar sources inside the
// newest creade slave node
 
// PARAMETER: OCP_APPLICATION_NAME is used to specify the name of application
// deployed inside Openshift
 
// PARAMETER: OCP_BUILD_PROJECT is used to specify the name of the Openshift
// project where the build is executed (generally the DEV environment)
 
// PARAMETER: OCP_IMAGE_VERSION is used to specify the version that the
// Openshift image must be built
 
// inside the configFileProvider method must be inserted, via shell command
// line, the commands that produce the artifact (ex. war, jar, ...).
// When the artifact is produced is retrieved the actual build config reference
// and compared with the new one, if a difference is found a patch on
// Build Config is patched with the newest image reference.
// The Source2Image instructions are done in order to start the build of the
// new image.
 
def packageBuild(def argsMap) {
    this.printVariables(argsMap)
 
    def customMvnSettings = "${argsMap['MAVEN_SETTINGS_ID']}"
    def mavenSettingId = (customMvnSettings?.trim()) ? customMvnSettings : "MAVEN_SETTINGS"
    def configVars  = argsMap['COMPONENT_CONFIG_VARS']
    def ocpProject  = "${argsMap['OCP_BUILD_PROJECT']}"
    def version     = "${argsMap['NEW_VERSION']}"
    def appName     = configVars.openshift.applicationName
     
 
     
    configFileProvider([configFile(fileId: mavenSettingId, variable: 'MAVEN_SETTINGS')]) {
        sh "mvn clean package -s ${MAVEN_SETTINGS} -DskipTests=true"
    }
 
    def oldImageReference = sh returnStdout: true, script: "echo \$(oc get bc ${appName} -n ${ocpProject} --template='{{ .spec.output.to.name }}')"
    def newImageReference = appName + ":" + version
 
    if (oldImageReference) {
        echo "${oldImageReference.trim()}"
        echo "${newImageReference}"
 
        if (oldImageReference.trim() != newImageReference) {
            sh "oc patch bc ${appName} -n ${ocpProject} -p \"{ \\\"spec\\\": { \\\"output\\\": { \\\"to\\\": { \\\"name\\\": \\\"${newImageReference}\\\" } } } }\""
        } else {
            echo "Same versions, no patching will be done"
        }
    }
 
    sh "mkdir -p ${WORKSPACE}//target/s2i-build/deployments\n" +
        "cp ${WORKSPACE}//target/*.war ${WORKSPACE}//target/s2i-build/deployments/\n" +
        "oc start-build ${appName} -n ${ocpProject} --follow=true --wait=true --from-dir=\"${WORKSPACE}//target/s2i-build\""
 
    echo "-----------------"
}
 
// printVariables() is a method used to print some environment variables inside
// the other methods
def printVariables(def argsMap) {
    for (item in argsMap) {
        echo "${item.key}: ${item.value}"
    }
}
 
 
 
 
def changeVersion(def info) {
    echo "Changing version to ${info['NEW_VERSION']}"
    configFileProvider([configFile(fileId: info['MAVEN_SETTINGS_ID'], variable: 'MAVEN_SETTINGS')]) {
        sh returnStatus: true, script: "mvn build-helper:parse-version versions:set -DnewVersion=${info['NEW_VERSION']} -DautoVersionSubmodules=true versions:commit -s ${MAVEN_SETTINGS}"
    }
 
    echo "Changed version to ${info['NEW_VERSION']}"
}
 
return this;