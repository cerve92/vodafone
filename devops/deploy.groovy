#!groovy
import io.devops.common.OpenshiftUtils
import io.devops.common.DockerUtils
import org.jenkinsci.plugins.pipeline.utility.steps.shaded.org.yaml.snakeyaml.DumperOptions
import org.jenkinsci.plugins.pipeline.utility.steps.shaded.org.yaml.snakeyaml.Yaml
 
def deploy(def argsMap){
    echo 'Starting deploy()...'
 
    def dockerUtils = new DockerUtils()
    def ocpUtils = new OpenshiftUtils()
 
    def version         = "${argsMap['DEPLOY_VERSION']}"
    def configVars      = argsMap['COMPONENT_CONFIG_VARS']
     
    def appName         = configVars.openshift.applicationName
 
    def branchName      = "${argsMap['COMPONENT_BRANCH']}"
    def repositoryUrl   = "${argsMap['COMPONENT_REPOSITORY']}"
    def environmentName = "${argsMap['TARGET_ENVIRONMENT']}"
    def ocpProjectName  = ocpUtils.getOCPProjectByEnv("${argsMap['METADATA']}", "${environmentName}")
    def updatedYamlFile
 
 
    def DEV_REGISTRY = "docker-registry.default.svc:5000"
    def INFRA_REGISTRY = "openshift-registrycluster.devops.vodafone.it:443"
 
        
 
    stage('Check ocp yaml file') {
        def yamlFileName
 
        if (fileExists('./ocp/ocp.yaml')) {
            yamlFileName = './ocp/ocp.yaml'
        } else if (fileExists('./ocp/ocp.yml')) {
            yamlFileName = './ocp/ocp.yml'
        } else {
            error "Cannot find any openshift yaml file (./ocp/ocp.yaml or ./ocp/ocp.yml not found)"
        }
 
        def myYaml = readYaml file: yamlFileName
        def dc
         
        //The yaml file could contain more objects separated by triple dash
        if (myYaml in List) {
            dc = ocpUtils.searchForDeploymentConfig(myYaml)
            if (dc==null) {
                error "Cannot find any DeploymentConfig in yaml ${yamlFileName}"
            }
        }
        else {
            dc = myYaml
            if (!ocpUtils.isDeploymentConfig(dc)) {
                error "Cannot find any DeploymentConfig in yaml ${yamlFileName}"
            }
        }
         
        def containers = dc.spec.template.spec.containers
        for (c in containers) {
            // A pod can contain more than one container spec
            // We update only the one that contains the appName
            if (c.image.contains(appName)) {
                echo "Found container with name '${c.name}' to be updated in DC"
                def oldImg = c.image
                def oldImgNoTag = dockerUtils.getImageBase(oldImg)
                def repoPath = dockerUtils.getImageRepoPath(oldImgNoTag)
 
                def dstImageUrl = "${DEV_REGISTRY}/${repoPath}:${version}"
 
                if (environmentName != "DEV") {
                    def srcImageUrl = "${INFRA_REGISTRY}/${repoPath}:${version}"
                     
                    echo "Pushing image from ${srcImageUrl} to ${dstImageUrl}..."
 
                    dockerUtils.mirrorImage(srcImageUrl, dstImageUrl)
                }
 
                //forcing cast to String to avoid error
                //!!org.codehaus.groovy.runtime.GStringImpl metaClass: !!groovy.lang.MetaClassImpl {} in produced yaml
                String dstImageUrlStr = (String) dstImageUrl
 
                c.image = dstImageUrlStr
                echo "Replaced old image '${oldImg}' with '${dstImageUrl}'"
 
            }
        }
 
        updatedYamlFile = yamlFileName + ".${env.BUILD_NUMBER}"
        //write the updated yaml in a temp file
        //writeYaml file: updatedYamlFile, data: myYaml
         
        yamlString = fromYamlToString(myYaml)
 
        print "-----------------FILE YAML----------------\n" + yamlString
         
        writeFile file: updatedYamlFile, text: yamlString
 
        //check the updated file
        echo "Printing the updated yaml file"
        sh "cat ${updatedYamlFile}"
    }
    stage('Update the OCP objects'){
        //echo "skipping the updatedYaml apply into OCP project ${ocpProjectName}..."
        openshift.withProject(ocpProjectName) {
            //echo "Exporting current ocp objects..."
            //def maps = openshift.selector( 'dc' )
            //def objs = maps.objects( exportable:true )
            //def exportFileName = "${ocpProjectName}-backup.yml"
            //writeYaml file: exportFileName, data: objs
            //echo "Exported previous ocp objects into ${exportFileName}:"
            //sh "cat ${exportFileName}"
             
            def dcPre = openshift.selector('dc',"${appName}")
            def idPre = dcPre.object().status.latestVersion
            echo "latest version pre apply: ${idPre}"
 
            echo "applying the updatedYaml into OCP project ${ocpProjectName}"
            def fromYaml = openshift.apply( readFile( updatedYamlFile ) )
             
            def dcPost = openshift.selector('dc',"${appName}")
            def idPost = dcPost.object().status.latestVersion
            echo "latest version post apply: ${idPost}"
 
            if(idPre.equals(idPost)){
                echo "pre and post latest versions are equal"
                dcPost.rollout().latest()
            }
        }
    }
    stage('Deployment Status Check'){
        ocpUtils.checkDCStatus(ocpProjectName,appName)
    }
}
 
@NonCPS
def fromYamlToString(myYaml) { 
    def sb = ""
    DumperOptions options = new DumperOptions()
    options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK)
    def yaml = new Yaml(options)
    if (myYaml in List) {
        sb=yaml.dumpAll(myYaml.iterator())
        // for (o in myYaml) {
        //     String output = yaml.dump(o)
        //     sb = sb + "---\n" + output
        // }
    }
    else{
        sb=yaml.dump(myYaml)
    }
    return sb
}
 
return this;