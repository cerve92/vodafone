import React from 'react';
import styles from './button.module.scss';

const Button = ({ label, style, enabled }) => {
  return <div className={`${styles.button} ${styles[style]}`}>{label}</div>;
};

export default Button;
