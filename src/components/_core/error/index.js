import React from 'react';
import styles from './error.module.scss';

const Error = ({ error }) => {
  console.log('props', error);

  return <div className={styles.error}>{error.message}</div>;
};

export default Error;
