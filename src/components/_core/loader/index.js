import React from 'react';
import loaderImg from './loading.svg';
import styles from './loader.module.scss';

const Loader = ({ type, rows }) => {
  // console.log("props", type, rows)

  return (
    <div className={styles.loader}>
      <img src={loaderImg} alt="Loading..." />
    </div>
  );
};

export default Loader;
