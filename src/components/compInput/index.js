import React from 'react';
import { connect } from 'react-redux';

import styles from './compInput.module.scss';

const mapProps = state => {
  return { list: state.list };
};

const mapDispatch = ({ list: { add, remove } }) => ({
  add: name => add('Senna'),
  remove: () => remove()
});

const compInput = props => {
  return (
    <div className={styles.content}>
      <button onClick={() => props.add()}>add driver</button>
      <button onClick={() => props.remove()}>remove driver</button>
    </div>
  );
};

export default connect(
  mapProps,
  mapDispatch
)(compInput);
