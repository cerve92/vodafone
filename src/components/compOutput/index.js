import React from 'react';
import { connect } from 'react-redux';

const mapProps = state => {
  return { list: state.list };
};

const compOutput = props => {
  return (
    <div>
      <h2>List driver</h2>
      {props &&
        props.list.map((e, i) => {
          return <div key={'test_' + i}>{e}</div>;
        })}
    </div>
  );
};

export default connect(mapProps)(compOutput);
