import styles from './editorial.module.scss';
import React, { Fragment } from 'react';
import Loader from 'components/_core/loader';
import Button from 'components/_core/button';

const Editorial = ({ aemDynamicDialogData }) => {
  const { title, description, background, buttons } = aemDynamicDialogData;

  return (
    <Fragment>
      {aemDynamicDialogData && (
        <div
          className={[styles.content, 'shadow', styles.loaded].join(' ')}
          style={{
            backgroundImage: 'url("' + background.image + '")',
            backgroundColor: background.color
          }}
        >
          <h2>{title}</h2>
          <p>{description}</p>
          {buttons && (
            <div className={styles.buttons}>
              {buttons &&
                buttons.map((e, i) => {
                  return <Button {...e} key={'btn_editorial_' + i} />;
                })}
            </div>
          )}
        </div>
      )}
      {!aemDynamicDialogData && <Loader />}
    </Fragment>
  );
};

export default Editorial;
