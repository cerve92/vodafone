import React from 'react';
import { connect } from 'react-redux';
import styles from './sim.module.scss';

import ricaricabile from './img/icon-ricaricabile.png';
import retefissa from './img/icon-retefissa.png';

const mapStateToProps = state => ({
  simList: state.simList.data,
  isLoading: state.simList.isLoading,
  error: state.simList.error
});

const mapDispatchToProps = ({ simList: { getSimList, setActiveSim } }) => ({
  getSimList: () => getSimList(),
  setActiveSim: payload => setActiveSim(payload)
});

const Sim = ({ props: { number, type, id, active }, setActiveSim }) => {
  return (
    <div
      className={[styles.content, active ? styles.active : ''].join(' ')}
      onClick={() => setActiveSim(id)}
    >
      <div className={styles.icon}>
        <img src={type.id === 1 ? ricaricabile : retefissa} alt={type.name} />
      </div>
      <div>
        <p className={styles.tipo}>{number}</p>
        <p className={styles.numero}>{type.name}</p>
      </div>
    </div>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sim);
