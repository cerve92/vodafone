import React, { Fragment } from 'react';
import Loader from 'components/_core/loader';
import { connect } from 'react-redux';
import styles from './simCounter.module.scss';

const mapStateToProps = state => ({
  simList: state.simList.data,
  simDetails: state.simDetails
});

const mapDispatchToProps = ({ simDetails: { getSimDetails } }) => ({
  getSimDetails: payload => getSimDetails(payload)
});

const SimCounter = props => {
  const { details, isLoading } = props.simDetails;

  return (
    <div className={[styles.content, 'shadow'].join(' ')}>
      {isLoading && <Loader type="text" rows={5} />}
      {!isLoading && (
        <Fragment>
          {details && (
            <Fragment>
              <h2>{details.plan.name}</h2>
              <p>Valido fino al {details.plan.duedate}</p>
              <div className={styles.rows}>
                {details.plan.items.map((e, i) => {
                  return (
                    <Fragment key={'items_' + i}>
                      <h3>{e.name}</h3>
                      <div className={styles.chartcontainer}>
                        <div
                          className={[styles.barchart, 'color_' + i].join(' ')}
                        >
                          <div
                            className={styles.progress}
                            style={{
                              width: Number((e.left * 100) / e.max) + '%'
                            }}
                          />
                        </div>
                        <div
                          className={styles.textchart}
                          style={{ color: e.left === e.max ? 'red' : '' }}
                        >
                          {e.left} di {e.max} {e.unit}
                        </div>
                      </div>
                    </Fragment>
                  );
                })}
              </div>
            </Fragment>
          )}
          {!details && (
            <Fragment>
              <h2>La tua offerta</h2>
              <p>Nessuna offerta attiva.</p>
            </Fragment>
          )}
        </Fragment>
      )}
    </div>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SimCounter);
