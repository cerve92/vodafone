import React, { Fragment } from 'react';
import Loader from 'components/_core/loader';
import { connect } from 'react-redux';
import styles from './simDetails.module.scss';

const mapStateToProps = state => ({
  simList: state.simList.data,
  simDetails: state.simDetails
});

const mapDispatchToProps = ({ simDetails: { getSimDetails } }) => ({
  getSimDetails: payload => getSimDetails(payload)
});

const SimDetails = props => {
  const { details, isLoading } = props.simDetails;

  return (
    <div className={[styles.content, 'shadow'].join(' ')}>
      {isLoading && <Loader type="text" rows={5} />}
      {!isLoading && (
        <Fragment>
          <h2>La mia Sim</h2>
          {details && (
            <Fragment>
              <div className={styles.columns}>
                <div className={styles.col}>
                  <label className={styles.label}>ATTIVAZIONE SIM</label>
                  <p>{details.activation}</p>
                </div>
                <div className={styles.col}>
                  <label className={styles.label}>SIM</label>
                  <p>{details.type}</p>
                </div>
                <div className={styles.col}>
                  <label className={styles.label}>CODICE PUK</label>
                  <p>{details.puk}</p>
                </div>

                {/* <div>{details.amount} {details.currency}</div>
						<div>{details.puk}</div> */}
              </div>
              <hr />
              <div className={styles.labelCredito}>Il mio credito residuo</div>
              <div className={styles.credRes}>
                {details.amount} {details.currency}
              </div>
            </Fragment>
          )}

          {!details && <p>Nessuna sim attiva.</p>}
        </Fragment>
      )}
    </div>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SimDetails);
