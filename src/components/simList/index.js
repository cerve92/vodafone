import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux';
import Loader from 'components/_core/loader';
import Modal from 'components/_core/modal';
import Error from 'components/_core/error';
import Sim from 'components/sim';
import styles from './simList.module.scss';

const mapStateToProps = state => ({
  simList: state.simList.data,
  isLoading: state.simList.isLoading,
  error: state.simList.error
});

const mapDispatchToProps = ({ simList: { getSimList } }) => ({
  getSimList: () => getSimList()
});

const simList = props => {
  useEffect(() => {
    props.getSimList();
  }, []);

  return (
    <Fragment>
      {props.error && (
        <Modal>
          <Error error={props.error} />
        </Modal>
      )}
      {props.isLoading && <Loader rows={2} type="media" />}
      {!props.error &&
        !props.isLoading &&
        props.simList &&
        props.simList.length > 0 && (
          <div className={styles.simList}>
            {props.simList.map((e, i) => {
              return <Sim props={e} key={'sim_' + i} />;
            })}
          </div>
        )}
      {!props.error && !props.isLoading && props.simList.length === 0 && (
        <div>No sim</div>
      )}
    </Fragment>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(simList);
