const BASE = process.env.REACT_APP_API_BASEURL;

module.exports = {
  GET_SIMS: BASE + '/getSims',
  GET_SIM_DETAILS: BASE + '/getSimDetails/',
  GET_CONTEXTHUB: BASE + '/getContextHub'
};
