/**
 * Configure Fetch
 *
 */
import axios from 'axios';

export default (URL, METHOD = 'POST', PARAMS = {}, TIMEOUT = 30000) => {
  const headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  };

  var configAxios = {
    method: METHOD,
    headers: headers,
    url: URL,
    params: PARAMS,
    timeout: TIMEOUT,
    crossDomain: true
  };

  if (
    METHOD.toUpperCase() === 'POST' ||
    METHOD.toUpperCase() === 'PUT' ||
    METHOD.toUpperCase() === 'PATCH'
  ) {
    configAxios = {
      ...configAxios,
      data: JSON.stringify(PARAMS)
    };
  }
  console.log('************')
  console.log(configAxios);
  console.log('***********')

  return axios(configAxios).catch(function(error) {
    console.log('FETCH ERROR', error.request);

    let newError = error.response || error.request;

    return newError.status;
    // if (error.response) {
    //   console.log("Response Error", error.response);

    // } else if (error.request) {
    //   console.log("Request Error", error.request);
    // } else {
    //   console.log('Error', error.message);
    // }
  });
};
