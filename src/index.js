import 'styles/main.scss';
import 'react-app-polyfill/ie11';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import ErrorBoundary from 'components/_core/ErrorBoundary';
import Loader from 'components/_core/loader';
import { init } from '@rematch/core';
import * as models from 'store';

const store = init({
  models
});

const { dispatch } = store;
dispatch.contexthub.getContextHub();

async function renderFunction(
  reactParentComponent,
  aemDivContainer,
  configs,
  aemDynamicDialogData
) {
  const DynamicLazyComponent = React.lazy(() =>
    import(
      /* webpackChunkName: "[request]" */ `components/${reactParentComponent}`
    )
  );

  ReactDOM.render(
    <Provider store={store}>
      <ErrorBoundary>
        <React.Suspense
          fallback={
            <Loader
              type={
                configs && configs.placeholder ? configs.placeholder.type : null
              }
              rows={3}
            />
          }
        >
          <DynamicLazyComponent aemDynamicDialogData={aemDynamicDialogData} />
        </React.Suspense>
      </ErrorBoundary>
    </Provider>,
    document.querySelector('#' + aemDivContainer)
  );
}

window.renderFunction = renderFunction;
