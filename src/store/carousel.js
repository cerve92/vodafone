export const carousel = {
  state: [
    {
      id: 1,
      title: 'Title1 AAA',
      description: 'Description1',
      imgSrc:
        'https://images.unsplash.com/photo-1527247043589-98e6ac08f56c?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjU4MTQ2fQ'
    },
    {
      id: 2,
      title: 'Title1',
      description: 'Description1',
      imgSrc:
        'https://images.unsplash.com/photo-1514867644123-6385d58d3cd4?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjU4MTQ2fQ'
    },
    {
      id: 3,
      title: 'Title1',
      description: 'Description1',
      imgSrc:
        'https://images.unsplash.com/photo-1536332016596-dc50468cbf41?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjU4MTQ2fQ'
    }
  ],
  reducers: {},
  effetcs: {}
};
