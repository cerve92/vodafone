import f from './../core/fetch.js';
import * as API from './../core/constants';

export const contexthub = {
  state: {
    isLoading: false,
    data: {},
    error: null
  },
  reducers: {
    setLoading(state, payload) {
      return {
        ...state,
        isLoading: true
      };
    },

    setError(state, payload) {
      return {
        ...state,
        isLoading: false,
        error: payload
      };
    },

    setData(state, data) {
      return {
        ...state,
        data: data,
        isLoading: false
      };
    }
  },

  effects: dispatch => ({
    async getContextHub() {
      dispatch.simDetails.setLoading();
      try {
        const result = await f(API.GET_CONTEXTHUB, 'GET');
        if (result.data.status !== 'ok') {
          dispatch.contexthub.setError('Error');
        } else {
          dispatch.contexthub.setData(result.data.data);
        }
      } catch (e) {
        dispatch.contexthub.setError('Error');
      }
    }
  })
};
