import f from './../core/fetch.js';
import * as API from './../core/constants';

export const simDetails = {
  state: {
    isLoading: false,
    data: {},
    error: null
  },
  reducers: {
    setLoading(state, payload) {
      return {
        ...state,
        isLoading: true
      };
    },

    setError(state, payload) {
      return {
        ...state,
        isLoading: false,
        error: payload
      };
    },

    setData(state, [idSim, data]) {
      return {
        ...state,
        details: data,
        isLoading: false
      };
    }
  },
  effects: dispatch => ({
    async getSimDetails(idSim, rootState) {
      dispatch.simDetails.setLoading();
      try {
/*         console.log('****************')
        console.log(API.GET_SIM_DETAILS + idSim)
        console.log('****************') */
        const result = await f(API.GET_SIM_DETAILS + idSim, 'GET');
        if (result.data.status !== 'ok') {
          dispatch.simDetails.setError('Error');
        } else {
          dispatch.simDetails.setData([idSim, result.data.data]);
        }
      } catch (e) {
        dispatch.simDetails.setError('Error');
      }
    }
  })
};
