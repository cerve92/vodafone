import f from './../core/fetch.js';
import * as API from './../core/constants';

export const simList = {
  state: {
    isLoading: false,
    data: [],
    error: null
  },
  reducers: {
    setLoading(state, payload) {
      return {
        ...state,
        isLoading: true
      };
    },

    setError(state, payload) {
      return {
        ...state,
        isLoading: false,
        error: payload
      };
    },

    setData(state, payload) {
      return {
        ...state,
        data: payload,
        isLoading: false
      };
    },

    setDetails(state, [idSim, data]) {
      return {
        ...state,
        data: [
          ...state.data.map(e => {
            if (e.id === idSim) {
              return {
                ...e,
                details: data
              };
            } else {
              return e;
            }
          })
        ],
        isLoading: false
      };
    },

    setActive(state, payload) {
      return {
        ...state,
        data: [
          ...state.data.map((e, i) => {
            return {
              ...e,
              active: e.id === payload
            };
          })
        ],
        isLoading: false
      };
    }
  },
  effects: dispatch => ({
    async getSimList(payload, rootState) {
      dispatch.simList.setLoading();
      try {
        const result = await f(API.GET_SIMS, 'GET');
        if (result && result.error) {
          dispatch.simList.setError(result);
        } else {
          if (result && result.data) {
            dispatch.simList.setData(result.data.data).then(() => {
              dispatch.simList.setActiveSim(result.data.data[0].id);
            });
          }
        }
      } catch (e) {
        console.log('Error', e);
        dispatch.simList.setError('Error');
      }
    },

    setActiveSim(payload, rootState) {
      dispatch.simDetails.setLoading();

      dispatch.simList.setActive(payload).then(() => {
        dispatch.simDetails.getSimDetails(payload);
      });
    }
  })
};
