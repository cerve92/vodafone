export const defaultA = {
    title: "Happy Black",
    description: "Vantaggi esclusivi su prodotti e servizi dei nostri partner, Giga illimitati per utilizzare le mappe sulle app che ami di più. E potrebbero arrivare altre sorprese!",
    background: {
      image: 'https://www.vodafone.it/portal/resources/media/Images/fdt_nba-bg/nba-bg2.jpg',
      color: '#455ff1'
    },
    buttons: [{
      label: "Più tardi",
      style: "white",
      enabled: true
    }, {
      label: "Attiva",
      style: "transparent",
      enabled: true
    }]
  };
  
export  const defaultB = {
    title: "Giga Week",
    description: "50 Giga sulla Giga Network 4.5G",
    background: {
      image: 'https://www.vodafone.it/portal/resources/media/Images/fdt_nba-bg/nba-bg3.jpg',
      color: '#ffaaf1'
    },
    buttons: [{
      label: "Più tardi",
      style: "white",
      enabled: true
    }, {
      label: "Attiva",
      style: "transparent",
      enabled: true
    }]
  };
  
export  const defaultC = {
    title: "Giga Weekend",
    description: "Giga illimitati sulla Giga Network 4.5G",
    background: {
      image: 'https://www.vodafone.it/portal/resources/media/Images/fdt_nba-bg/nba-bg4.jpg',
      color: '#f15670'
    },
    buttons: [{
      label: "Più tardi",
      style: "white",
      enabled: true
    }, {
      label: "Attiva",
      style: "transparent",
      enabled: true
    }]
  };

