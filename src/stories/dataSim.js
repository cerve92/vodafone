export const defaultSim = {
    "number": "3480000001",
    "type": {
        "id": 1,
        "name": "Ricaricabile"
    }, "id": 123
};

export const defaultSims = [
    {
        "number": "3480000001",
        "type": {
            "id": 1,
            "name": "Ricaricabile"
        }, "id": 123
    },
    {
        "number": "3487866689",
        "type": {
            "id": 2,
            "name": "Ricaricabile"
        }, "id": 127
    },
    {
        "number": "010296877",
        "type": {
            "id": 3,
            "name": "Fisso"
        }, "id": 131
    }
]