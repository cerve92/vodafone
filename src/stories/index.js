import 'styles/main.scss';
import React from 'react';
import { Provider } from 'react-redux';
import Loader from 'components/_core/loader';
import ErrorBoundary from 'components/_core/ErrorBoundary';
import { init } from '@rematch/core';
import * as models from 'store';

import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, object, number, color, files } from '@storybook/addon-knobs';

import Sim from 'components/sim'

import * as DataEditorial from './dataEditorial.js';
import * as DataSim from './dataSim.js';



const store = init({
  models
});

const { dispatch } = store;

const addStoriesKnobs = (categoryKnobs, nameComponent, titleChildMenu, typePlaceholder) => {
  return (
    storiesOf(categoryKnobs, module)
      .addDecorator(withKnobs)
      .add(titleChildMenu, () => renderFunctionStorybook(nameComponent, {
        placeholder: {
          type: typePlaceholder
        }
      }))
  )
}

const addStoriesEditorial = (categoryKnobs, obj) =>
  storiesOf(categoryKnobs, module)
    .addDecorator(withKnobs)
    .add(obj.title, () => {
      
      return editorial(parseObjectToKnobs(obj), true, store);
    });



const parseObjectToKnobs = (obj) => {
  let arrayButton = [];
  try {
    Object(obj.buttons).forEach(function (e, i) {
      arrayButton.push(object(`btn-${i}`, {
        label: text(`label-${i}`, e.label, 'Buttons'),
        style: e.style,
        enabled: boolean(`enabled-${i}`, e.enabled, 'Buttons')
      }, 'Style'));
    })


    return (
      object('Data', {
        title: text('Title', obj.title),
        description: text('Description', obj.description),
        background: object('background', {
          image: files('image', [], obj.background.image, 'Background'),
          color: color('color', obj.background.color, 'Background')
        }, 'Style'),
        buttons: arrayButton
      })
    )
  }
  catch(e){
    console.log(e)
  }
}

/* function extract(data, where) {
  for (var key in data) {
    where[key] = data[key];
  }
}

function insieme() {
  extract(defaultDataEditorialA, this)
  alert(title);
} */

const defaultDataEditorialA = DataEditorial.defaultA
const defaultDataEditorialB = DataEditorial.defaultB
const defaultDataEditorialC = DataEditorial.defaultC

/*async*/ function DynamicLazyComponentStoryBook(reactParentComponent, aemDynamicDialogData) {
  const DynamicLazyComponent = React.lazy(() =>
    import(
      /* webpackChunkName: "[request]" */ `components/${reactParentComponent}`
    )
  );
  return (<DynamicLazyComponent aemDynamicDialogData={aemDynamicDialogData} />)
}

const editorial = (dataEditorial, boolean, store) => {
  const flag = boolean;
  if (flag) {
    return (
      <Provider store={store}>
        <React.Suspense
          fallback={<Loader rows={3} />}
        >
          {DynamicLazyComponentStoryBook('editorial', dataEditorial)}
        </React.Suspense>
      </Provider>
    );
  }
  return DynamicLazyComponentStoryBook('editorial', dataEditorial);
}

const renderFunctionStorybook = (
  reactParentComponent,
  configs,
  aemDynamicDialogData
) => {
  const DynamicLazyComponent = React.lazy(() =>
    import(
      /* webpackChunkName: "[request]" */ `components/${reactParentComponent}`
    )
  );
  dispatch.contexthub.getContextHub();
  return (
    <Provider store={store}>
      <ErrorBoundary>
        <React.Suspense
          fallback={<Loader type={
            configs && configs.placeholder ? configs.placeholder.type : null
          } rows={3} />}
        >
          <DynamicLazyComponent aemDynamicDialogData={aemDynamicDialogData} />
        </React.Suspense>
      </ErrorBoundary>
    </Provider>
  );
}

storiesOf('Vodafone', module)
  .add('Vodafone', () => <Provider store={store}>
    <ErrorBoundary>
      <React.Suspense fallback={<Loader rows={3} />}>
        {DynamicLazyComponentStoryBook('simList')}
        {DynamicLazyComponentStoryBook('simDetails')}
        <hr />
        {DynamicLazyComponentStoryBook('simCounter')}
        <hr />
        {editorial(defaultDataEditorialA)}
        <hr />
        {editorial(defaultDataEditorialB)}
        <hr />
        {editorial(defaultDataEditorialC)}

      </React.Suspense>
    </ErrorBoundary>
  </Provider>);

storiesOf('Components', module)
  .add('Loading', () => <Loader />);

storiesOf('Components', module)
  .addDecorator(withKnobs)
  .add('Sim', () => {
    const defaultDataSim = object('dataSim', DataSim.defaultSim);
    const keyDataSim = number('Key', 0);

    return (
      <Provider store={store}>
        <Sim props={defaultDataSim} key={keyDataSim} />
        <hr />
        <div>
          <p>{`<Sim props={Data} key={i} />`}</p><br />
          <p>{`Data = ${JSON.stringify(defaultDataSim)}`}</p><br />
          <p>{`key: ${keyDataSim}`}</p>
        </div>
      </Provider>);
  });

addStoriesKnobs('Components', 'simList', 'Sim List', 'media');
addStoriesKnobs('Components', 'simDetails', 'Sim Details', 'text');
addStoriesKnobs('Components', 'simCounter', 'Sim Counter', 'media');

addStoriesEditorial('Components', defaultDataEditorialA);
addStoriesEditorial('Components', defaultDataEditorialB);
addStoriesEditorial('Components', defaultDataEditorialC);

